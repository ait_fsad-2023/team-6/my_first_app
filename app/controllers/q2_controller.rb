class Q2Controller < ApplicationController
  def index
    begin
      # Add code to log a message before dividing by zero
      Rails.logger.error("About to divide by 0")
      
      # Generate a divide-by-zero exception
      result = 5 / 0

      # The exception will prevent this code from running
    rescue StandardError => e
      # If an exception occurs, set an error message
      @error_message = "An error occurred: #{e.message}"
    end
  end
end

