require "test_helper"

class HeadlinesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get headlines_index_url
    assert_response :success
  end
end
