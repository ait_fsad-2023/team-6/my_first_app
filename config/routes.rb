Rails.application.routes.draw do
 
  get 'problemset1', to: 'problemset1#subpage'
  get 'q1', to: 'q1#show'
  get 'q3', to: 'news#index'

  get 'q2', to: 'q2#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root 'main_site#index'

  
  # Defines the root path route ("/")
  # root "articles#index"
end
